import sys
from pydantic import BaseConfig
from decouple import config

_DB_USER = config('DB_USER')
_DB_PASSWORD = config('DB_PASSWORD')
_DB_NAME = config('DB_NAME')
_DB_HOST = config('DB_HOST')
_DB_PORT = config('DB_PORT', default=5432)

_DB_URL = f'postgres://{_DB_USER}:{_DB_PASSWORD}@{_DB_HOST}:{_DB_PORT}/{_DB_NAME}'

_DB_TEST_URL = 'sqlite://:memory:'

class __Config(BaseConfig):
    DATABASE_URL = _DB_TEST_URL
    DATABASE_URL = _DB_TEST_URL if 'pytest' in ''.join(sys.argv) else _DB_URL


settings = __Config()