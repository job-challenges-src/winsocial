# mypy: no-disallow-untyped-decorators
# pylint: disable=E0611,E0401
import asyncio
from typing import Generator

import pytest

from fastapi import status
from fastapi.testclient import TestClient
from tortoise.contrib.test import finalizer, initializer

from settings.main import app


@pytest.fixture(scope='module')
def client() -> Generator:
    initializer(['src.models'])
    with TestClient(app) as test_client:
        yield test_client
    finalizer()


@pytest.fixture(scope='module')
def event_loop():
    yield asyncio.get_event_loop()


def test_list_leads(client: TestClient):
    response = client.get(f'/lead')
    assert response.status_code == status.HTTP_200_OK

@pytest.mark.anyio
async def test_create_lead(client: TestClient):
    response = client.post(f'/lead',
        json={
            'nome': 'Irineu',
            'telefone': '986656498562'
        }
    )
    assert response.status_code == status.HTTP_201_CREATED

def test_list_salesmans(client: TestClient):
    response = client.get(f'/salesman')
    assert response.status_code == status.HTTP_200_OK

@pytest.mark.anyio
async def test_create_salesman(client: TestClient):
    response = client.post(f'/salesman',
        json={
            "nome": "Vendedor pleno",
            "nivel": 2
        }
    )
    assert response.status_code == status.HTTP_201_CREATED