from datetime import datetime, timezone
from dateutil.relativedelta import relativedelta
from src.utils.datetime import convert_seconds_to_hours, get_hours_between_two_dates


def test_convert_seconds_to_hours():
    HOUR_IN_SECONDS = 3600
    assert convert_seconds_to_hours(HOUR_IN_SECONDS) == 1


def test_get_hours_between_two_dates():
    end_date = datetime.now(timezone.utc) 
    start_date = end_date - relativedelta(hours=2, minutes=30)
    number_hours = get_hours_between_two_dates(start_date, end_date)
    assert number_hours == 2.5
