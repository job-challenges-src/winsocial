from datetime import datetime, timezone
from dateutil.relativedelta import relativedelta

from src.models import Salesman
from src.enums import SalesmanLevelEnum

"""
    O SCORE de um vendedor
    Pleno que não recebe um lead há 3 horas será de 4.5.
    Por outro lado, um sênior que não recebe um lead há 2.5 horas terá um SCORE de 5. Ou seja, o vendedor sênior receberia o lead por ter um SCORE mais alto.
"""

def test_score_pleno():
    salesman = Salesman()
    current_date = datetime.now(timezone.utc)
    quantity_of_past_hours = 3
    received_at = current_date - relativedelta(hours=quantity_of_past_hours)
    
    salesman.level = SalesmanLevelEnum.PLENO.value
    salesman.last_lead_received_at = received_at
    assert salesman.score(current_date) == 4.5

def test_score_senior():
    salesman = Salesman()
    current_date = datetime.now(timezone.utc)
    quantity_of_past_hours = 2
    quantity_of_add_minutes = 30
    
    received_at = current_date - relativedelta(hours=quantity_of_past_hours, minutes=quantity_of_add_minutes)
    
    salesman.level = SalesmanLevelEnum.SENIOR.value
    salesman.last_lead_received_at = received_at

    assert salesman.score(current_date) == 5