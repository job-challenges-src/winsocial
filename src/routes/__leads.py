from typing import List
from fastapi import APIRouter

from src.schemas import LeadOut, SalesmanOut
from src.controllers.__lead_controller import list_lead, list_better_salesman, crete_lead

_router = APIRouter()

_router.add_api_route(
    path='/lead',
    methods=['GET'],
    endpoint=list_lead,
    name='Listar leads',
    response_model=List[LeadOut]
)

_router.add_api_route(
    path='/lead/{lead_id}/salesmans',
    methods=['GET'],
    endpoint=list_better_salesman,
    name='Agentes mais qualificados',
    description='Endpoint para retornar os agentes mais qualificados para um lead específico',
    response_model=List[SalesmanOut]
)

_router.add_api_route(
    path='/lead',
    methods=['POST'],
    status_code=201,
    endpoint=crete_lead,
    name='Cadastrar lead',
    description='Endpoint para cadastrar lead com nome e telefone',
    response_model=LeadOut
)
