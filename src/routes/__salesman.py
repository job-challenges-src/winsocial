from typing import List
from fastapi import APIRouter

from src.schemas import SalesmanOut
from src.controllers.__salesman_controller import list_salesman, crete_salesman

_router = APIRouter()

_router.add_api_route(
    path='/salesman',
    methods=['GET'],
    endpoint=list_salesman,
    name='Listar agentes',
    response_model=List[SalesmanOut]
)

_router.add_api_route(
    path='/salesman',
    methods=['POST'],
    status_code=201,
    endpoint=crete_salesman,
    name='Cadastrar agente',
    description='Endpoint para cadastrar agente com seus nomes e níveis de senioridade',
    response_model=SalesmanOut
)
