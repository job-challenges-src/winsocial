from fastapi import APIRouter

from .__leads import _router as __leads_router
from .__salesman import _router as __salesman_router

route = APIRouter()

route.include_router(__leads_router, tags=['Lead'])
route.include_router(__salesman_router, tags=['Salesman'])
