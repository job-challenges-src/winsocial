from uuid import UUID
from typing import List
from pydantic import BaseModel
from src.models import Lead

class LeadInput(BaseModel):
    nome: str
    telefone: int

    def to_orm(self) -> Lead:
        return Lead(
            name=self.nome,
            phone=self.telefone
        )

class LeadOut(LeadInput):
    id: UUID
    
    @classmethod
    def from_orm(self, obj: Lead):
        return LeadOut(
            id=obj.id,
            nome=obj.name,
            telefone=obj.phone
        )


class LeadResponse(BaseModel):

    @classmethod
    def from_queryset(self, queryset: List[Lead]) -> List[LeadOut]:
        data: List[LeadOut] = [LeadOut.from_orm(salesman) for salesman in queryset]
        data.sort(key=lambda i: i.nome)
        return data
