from uuid import UUID
from pytz import timezone
from datetime import datetime
from typing import List
from pydantic import BaseModel
from src.models import Salesman

class SalesmanInput(BaseModel):
    nome: str
    nivel: int

    def to_orm(self) -> Salesman:
        return Salesman(
            name=self.nome,
            level=self.nivel
        )

class SalesmanOut(SalesmanInput):
    score: float
    ultimo_lead_recebido: datetime
    id: UUID
    

    @classmethod
    def from_orm(self, obj: Salesman):
        return SalesmanOut(
            id=obj.id,
            nome=obj.name,
            nivel=obj.level, 
            ultimo_lead_recebido=obj.last_lead_received_at.astimezone(timezone('America/Sao_Paulo')),
            score=obj.score()
        )



class SalesmanResponse(BaseModel):

    @classmethod
    def from_queryset(self, queryset: List[Salesman]) -> List[SalesmanOut]:
        data: List[SalesmanOut] = [SalesmanOut.from_orm(salesman) for salesman in queryset]
        data.sort(key=lambda i: i.score, reverse=True)
        return data
