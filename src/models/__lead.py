from tortoise import fields, models

class Lead(models.Model):
    id = fields.UUIDField(pk=True, index=True)
    name = fields.CharField(max_length=120)
    phone = fields.CharField(max_length=15)