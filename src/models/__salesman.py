
from datetime import datetime, timezone
from typing import Optional

from tortoise import fields, models

from src.enums import SalesmanLevelEnum
from src.utils.datetime import get_hours_between_two_dates


class Salesman(models.Model):
    id = fields.UUIDField(pk=True, index=True)
    name = fields.CharField(max_length=70)
    level = fields.IntEnumField(enum_type=SalesmanLevelEnum)
    last_lead_received_at = fields.DatetimeField(default=lambda: datetime.now())

    def score(self, current_date: datetime = None) -> float:
        """
            SCORE = hours_without_received * level_value onde:
            hours_without_received é o número de horas úteis desde o último recebimento de um lead
            level_value é o multiplicador associado à senioridade do vendedor.
        """
        level_value: float = SalesmanLevelEnum.get_level_value(self.level)
        hours_without_received = get_hours_between_two_dates(self.last_lead_received_at, current_date)
        return round(hours_without_received * level_value, 2)

    async def delivery_lead(self, delivered_at: Optional[datetime] = None):
        if not delivered_at:
            delivered_at = datetime.now(timezone.utc)

        self.last_lead_received_at = delivered_at
        self.save()