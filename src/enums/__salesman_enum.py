from enum import IntEnum
from typing import Dict

class SalesmanLevelEnum(IntEnum):
    JUNIOR = 1
    PLENO =  2
    SENIOR = 3
    
    @classmethod
    def contains(cls, value: int) -> bool:
        return value in cls.__members__.values()
    
    @classmethod
    def get_level_value(cls, level: int) -> float:
        __level_values: Dict[int, float] = {
            cls.JUNIOR.value: 1.0,
            cls.PLENO.value: 1.5,
            cls.SENIOR.value: 2.0
        }
        if not cls.contains(level):
            return __level_values.get(cls.JUNIOR.value)
        return __level_values.get(level)
