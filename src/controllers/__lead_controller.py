

from typing import List

from fastapi import HTTPException, status
from src.schemas import LeadInput, LeadOut, LeadResponse, SalesmanOut, SalesmanResponse
from src.models import Lead, Salesman


async def list_lead() -> List[LeadOut]:
    queryset = await Lead.all()
    return LeadResponse.from_queryset(queryset)

async def list_better_salesman(lead_id: str) -> List[SalesmanOut]:
    lead = await Lead.get_or_none(id=lead_id)
    if not lead:
        raise HTTPException(
            detail='Lead does not exists',
            status_code=status.HTTP_404_NOT_FOUND
        )
        
    queryset = await Salesman.all()
    return SalesmanResponse.from_queryset(queryset)


async def crete_lead(input: LeadInput) -> LeadOut:
    lead = input.to_orm()
    await lead.save()
    return LeadOut.from_orm(lead)


