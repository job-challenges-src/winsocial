

from typing import List

from fastapi import HTTPException, status
from src.schemas import SalesmanInput, SalesmanOut, SalesmanResponse
from src.models import Lead, Salesman


async def list_salesman() -> List[SalesmanOut]:
    queryset = await Salesman.all()
    return SalesmanResponse.from_queryset(queryset)

async def list_better_salesman(lead_id: str) -> List[SalesmanOut]:
    lead = await Lead.get_or_none(id=lead_id)
    if not lead:
        raise HTTPException(
            detail='Lead does not exists',
            status_code=status.HTTP_404_NOT_FOUND
        )
        
    queryset = await Salesman.all()
    return SalesmanResponse.from_queryset(queryset)


async def crete_salesman(input: SalesmanInput) -> SalesmanOut:
    salesman = input.to_orm()
    await salesman.save()
    return SalesmanOut.from_orm(salesman)


