import json
from datetime import datetime, timezone
from random import randint

class ImportData:
    
    @classmethod
    async def exec(self):
        from src.models import Salesman, Lead
        
        with open('leads.json', 'r') as file:
            leads = json.loads(file.read())
            print('Criando leads')
            for data in leads:
                if not await Lead.filter(name=data.get('name')).exists():
                    lead = await Lead.create(**data)
                    print('\t', lead.name)
        
        with open('salesman.json', 'r') as file:
            salesmans = json.loads(file.read())
            print('Criando salesman')
            for data in salesmans:
                if not await Salesman.filter(name=data.get('name')).exists():
                    data['last_lead_received_at'] = datetime.now(timezone.utc).replace(hour=randint(1, 23))
                    salesman = await Salesman.create(**data)
                    print('\t', salesman.name)
