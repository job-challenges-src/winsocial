from datetime import datetime, timedelta, timezone
from typing import Optional

def convert_seconds_to_hours(seconds: int) -> float:
    """
        Convert seconds in hours.
        Example:
            3600 seconds == 1 hour
        
    """
    HOUR_IN_SECONDS = 3600
    HOUR_IN_MINUTES = 60
    MINUTE_IN_SECONDS = 60
    if seconds < HOUR_IN_SECONDS:
        return 0
    return round((seconds // MINUTE_IN_SECONDS) / HOUR_IN_MINUTES, 2)


def get_hours_between_two_dates(
    start_date: datetime,
    end_date: Optional[datetime] = None
    ) -> float:
        """
            Returns number of hours between two dates
            @params:
                start_date: datetime -> oldest date
                end_date: datetime = datetime.now() -> newest date
        """
        if not end_date:
            end_date = datetime.now()
        received_at: timedelta =  end_date.astimezone(timezone.utc) - start_date.astimezone(timezone.utc)
        return convert_seconds_to_hours(received_at.seconds)