# Desafio WinSocial - Arquiteto(a) de Sistemas

## Getting started

Para rodar o projeto você precisa ter instalado o `docker` e `docker-compose`.

Para iniciar o projeto você precisa executar somente um simples comando.

```bash
sudo docker-compose up
```
ou caso queira utilizar o alias do projeto.

```bash
make up
```

Para rodar os testes unitários:

```bash
sudo docker-compose run app pytest -x -s -v
```
ou caso queira utilizar o alias do projeto.

```bash
make test
```

## Documentação
A documentação da api está disponível em

[Swagger](http://localhost:8000/docs) <br>
[Redoc](http://localhost:8000/redoc)

###  Roadmap

- [✔️] Endpoint para cadastrar agentes com seus nomes e níveis de senioridade.
- [✔️] Endpoint para cadastrar leads com nome e telefone.

- [✔️] Endpoint para retornar os agentes mais qualificados para um lead específico (ordenados pelo score de forma decrescente).